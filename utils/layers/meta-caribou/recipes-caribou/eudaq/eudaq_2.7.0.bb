DESCRIPTION = "EUDAQ module"

S = "${WORKDIR}/git"

LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${S}/LICENSE.md;md5=e6a600fd5e1d9cbde2d983680233ad02"

SRC_URI = "gitsm://github.com/eudaq/eudaq.git;protocol=https"
SRCREV = "d2182caf73e0c257d45acf3e6d115ae97efc1972"

# Some directories:
# ${S}: source directory
# ${B}: build directory

inherit cmake pkgconfig

DEPENDS += "peary"

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = " -DUSER_CARIBOU_BUILD=ON \
                  -DEUDAQ_BUILD_GUI=OFF \
                  -DEUDAQ_BUILD_ONLINE_ROOT_MONITOR=OFF \
                  -DEUDAQ_INSTALL_PREFIX=/usr/ \
                  -DCMAKE_PREFIX_PATH=/usr/share/cmake/Modules/ \
                  -DCMAKE_SKIP_INSTALL_RPATH=TRUE \
                "

FILES_SOLIBSDEV = ""
FILES:${PN} += "${FILES_SOLIBSDEV}"
FILES:${PN} += "${libdir}/*"

FILES:${PN}-dev += "/usr/cmake/*"
FILES:${PN}-dev += "${includedir}"

# FIXME at some point properly remove the CMake target files generated in the build directory
FILES:${PN}-dev += "${B}/*.cmake"

INSANE_SKIP:${PN} = "dev-so"

LDFLAGS=""
