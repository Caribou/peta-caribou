DESCRIPTION = "OSL image definition for Xilinx boards"
LICENSE = "MIT"

require caribou-image.inc

IMAGE_INSTALL += " \
  vim \
  cmake \
  git \
  subversion \
  python3 \
  python3-numpy \
  python3-pandas \
  python3-cycler \
  python3-pyparsing \
  python3-matplotlib \
  nfs-utils \
  sysstat \
  gdb \
  i2c-tools \
  i2c-tools-dev \
  nano \
  screen \
  peary \
  eudaq \
  ntp \
  sshfs-fuse \
"

