DESCRIPTION = "DAQ framework for the Carioub DAQ System"

S = "${WORKDIR}/git"

LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${S}/LICENSE.md;md5=c160dd417c123daff7a62852761d8706"

SRC_URI = "gitsm://gitlab.cern.ch/Caribou/peary.git;protocol=https"
SRCREV = "1d2d723a895aef8118c071e7cf6206c5d82470e9"

inherit cmake pkgconfig

DEPENDS = "i2c-tools libiio readline"

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = " -DBUILD_example=OFF \
                  -DINTERFACE_MEDIA=OFF \
                  -DINTERFACE_IIO=OFF \
                  -DCMAKE_INSTALL_PREFIX=/usr/ \
		              -DCMAKE_SKIP_RPATH=ON \
		              -DCMAKE_BUILD_TYPE=Release \
		              -DBUILD_server=ON \
                  -DBUILD_C1004=OFF \
	       "
FILES:${PN} += "${FILES_SOLIBSDEV}"
FILES:${PN} += "${libdir}/*"
FILES:${PN} += "/usr/etc/*"
FILES:${PN} += "${bindir}/*"
FILES:${PN}-dev = "${includedir} /usr/share/*"

INSANE_SKIP:${PN} = "dev-so"
