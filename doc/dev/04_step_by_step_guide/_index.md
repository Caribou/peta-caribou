---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Step-by-step Guide"
weight: 3
---

## Step 1 : Create a PetaLinux Project
Once PetaLinux is installed, you need to create a new project. We will be using the Board Support (BSP) files provided by AMD-Xilinx. You can find them as well on the [downloads](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html)

Create the project:
```
$ petalinux-create --type project --source <path/to/the/.bsp/file> --name <your-project-name>
$ cd <your-project-name>
```

Here, `<path/to/the/.bsp/file>` is the path where the BSP file was downloaded, and `<your-project-name>` is your chosen name for the project.

<!-- #FIXME Still need to figure out if this step is needed -->
<!-- ## Step 2 : Import Hardware Description
You need to import the hardware description from Vivado:
```
$ petalinux-config --get-hw-description <path-to-hardware-description-directory> --silentconfig
``` -->

## Step 2: Configure project

### Network Configuration:

Use petalinux-config to open the kernel and system configuration menus:
```
$ petalinux-config
```

In `Subsystem AUTO Hardware Settings > Ethernet Settings`: 
* Set Ethernet MAC address for example: 00:0A:35:00:01:22.
* Enable obtaining IP address automatically

### Image packaging method

In `Image Packaging Configuration`:
* Set Root filesystem type to EXT4 (SD/eMMC/SATA/USB)

<!-- ### Root File-System Configuration:
Use petalinux-config to opent the root file-system configuration menu:
```
$ petalinux-config -c rootfs
```

In `Image Features` enable the auto-login support.
In `PetaLinux RootFS Settings` change `root:root` to `root:` in order to disable password for `root` 
This will throw a warning that no password was set for root, but let's keep it like this for now. -->

## Step 3 : Add Caribou layer

Copy the premade meta-caribou layer from the utils folder. 
```
$ cp -r utils/layers/<zc706|zcu102>/meta-caribou <your-project-path>/project-spec/
```
Make sure to choose either the `zc706` or `zcu102` layer.

Details about the content of each layer file is given in the "[Create Caribou layer tree from scratch](#create-caribou-layer-tree-from-scratch)" section. 

### Add meta-caribou layer to project
Finally, add the `meta-caribou` layer to the built layers. Open the petalinux configuration menu:
```
$ petalinux-config
```

In `Yocto Settings > User Layers` add `${PROOT}/project-spec/meta-caribou` as a user layer

## Step 5 : Build the project
Compile your entire system with:
```
$ petalinux-build -c caribou-image
```

## Step 6 : Package the image for SD card
After building, create a bootable image suitable for an SD card:
### Generate Boot Files:
```
$ petalinux-package --boot --fsbl images/linux/<FSBL-ELF-file> --u-boot images/linux/u-boot.elf --fpga --force
$ petalinux-package --wic
```
Where `<FSBL-ELF-file>` is:
- `zynq_fsbl.elf` for ZC706
- `zynqmp_fsbl.eld` for ZCU102

### Copy boot loaders and Linux image to SD card

Connect your SD card. It should show up as `/dev/sdX`. For example `/dev/sdc` 
Delete all partitions:
```
$ sudo wipefs --force -a <your-device>
```

Write image to SD card:
```
$ sudo dd if=images/linux/petalinux-sdimage.wic of=/dev/sdc bs=1M
$ sudo sync
```

Update device list:
```
$ sudo partprobe
```

## Step 8 : Boot your board
Connect the SD card to your board and make sure that the relevant switches are such that the board is configured to boot from the SD card

# Your board should be on now and pingable! :)


## Create Caribou layer tree from scratch:
Create the folder tree for the `meta-caribou` layer

```
$ cd project-spec
$ mkdir meta-caribou 
$ mkdir meta-caribou/{conf,recipes-caribou,recipes-bsp}
$ touch meta-caribou/conf/layer.conf
$ mkdir meta-caribou/recipes-caribou/{peary,eudaq,images}
$ touch meta-caribou/recipes-caribou/peary/peary_git.bb
$ touch meta-caribou/recipes-caribou/eudaq/eudaq_git.bb
$ touch meta-caribou/recipes-caribou/images/{caribou-image,caribou-image.inc}
$ mkdir meta-caribou/recipes-bsp/device-tree
$ mkdir meta-caribou/recipes-bsp/device-tree/files
$ touch meta-caribou/recipes-bsp/device-tree/device-tree.bbappend 
$ touch meta-caribou/recipes-bsp/device-tree/files/system-caribou.dtsi
```

Fill the created files

Fill the `layer.conf` file with the following content:
```
# We have a conf and classes directory, add to BBPATH
BBPATH .= ":${LAYERDIR}"

# We have recipes-* directories, add to BBFILES
BBFILES += "${LAYERDIR}/recipes-*/*/*.bb \
	${LAYERDIR}/recipes-*/*/*.bbappend"

# Define dynamic layers
BBFILES_DYNAMIC += " \
xilinx-tools:${LAYERDIR}/meta-xilinx-tools/recipes-*/*/*.bbappend \
"

BBFILE_COLLECTIONS += "meta-caribou"

BBFILE_PATTERN_meta-caribou = "^${LAYERDIR}/"
BBFILE_PRIORITY_meta-caribou = "6"
LAYERSERIES_COMPAT_meta-caribou = "honister"

LAYERDEPENDS_meta-caribou = "xilinx"
LAYERDEPENDS_meta-caribou = "xilinx-contrib"
LAYERDEPENDS_meta-caribou = "openembedded-layer"
```

Fill the `peary_git.bb` file with the following content:
```
DESCRIPTION = "DAQ framework for the Carioub DAQ System"

S = "${WORKDIR}/git"

LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${S}/LICENSE.md;md5=c160dd417c123daff7a62852761d8706"

SRC_URI = "gitsm://gitlab.cern.ch/Caribou/peary.git;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"


inherit cmake pkgconfig

DEPENDS = "i2c-tools libiio readline"

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = " -DBUILD_example=OFF \
                  -DINTERFACE_MEDIA=OFF \
                  -DINTERFACE_IIO=OFF \
                  -DCMAKE_INSTALL_PREFIX=/usr/ \
		              -DCMAKE_SKIP_RPATH=ON \
		              -DCMAKE_BUILD_TYPE=Release \
		              -DBUILD_server=ON \
                  -DBUILD_C1004=OFF \
	       "
FILES:${PN} += "${FILES_SOLIBSDEV}"
FILES:${PN} += "${libdir}/*"
FILES:${PN} += "usr/etc/*"
FILES:${PN} += "${bindir}/*"
FILES:${PN}-dev = "${includedir} usr/share/*"

INSANE_SKIP:${PN} = "dev-so"
```

Fill the `eudaq_git.bb` file with the following content:
```
DESCRIPTION = "EUDAQ module"

S = "${WORKDIR}/git"

LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${S}/LICENSE.md;md5=e6a600fd5e1d9cbde2d983680233ad02"

SRC_URI = "gitsm://github.com/eudaq/eudaq.git;protocol=https"

# Modify these as desired
PV = "0.1+git${SRCPV}"
SRCREV = "v2.4.7"

# Some directories:
# ${S}: source directory
# ${B}: build directory

inherit cmake pkgconfig

DEPENDS += "peary"

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = " -DUSER_CARIBOU_BUILD=ON \
                  -DEUDAQ_BUILD_GUI=OFF \
                  -DEUDAQ_BUILD_ONLINE_ROOT_MONITOR=OFF \
                  -DEUDAQ_INSTALL_PREFIX=/usr/ \
                  -DCMAKE_PREFIX_PATH=/usr/share/cmake/Modules/ \
                  -DCMAKE_SKIP_INSTALL_RPATH=TRUE \
                "

FILES_SOLIBSDEV = ""
FILES:${PN} += "${FILES_SOLIBSDEV}"
FILES:${PN} += "${libdir}/*"

FILES:${PN}-dev += "usr/cmake/*"
FILES:${PN}-dev += "${includedir}"

# FIXME at some point properly remove the CMake target files generated in the build directory
FILES:${PN}-dev += "${B}/*.cmake"

INSANE_SKIP:${PN} = "dev-so"

LDFLAGS=""
```

Fill the `caribou-image.bb` file with the following content:
```
DESCRIPTION = "OSL image definition for Xilinx boards"
LICENSE = "MIT"

require caribou-image.inc

IMAGE_INSTALL += " \
  vim \
  cmake \
  git \
  subversion \
  python3 \
  python3-numpy \
  python3-pandas \
  python3-cycler \
  python3-pyparsing \
  python3-matplotlib \
  nfs-utils \
  sysstat \
  gdb \
  i2c-tools \
  i2c-tools-dev \
  nano \
  screen \
  peary \
  eudaq \
  ntp \
  sshfs-fuse \
"
```

Fill the `caribou-image.inc` with the following content:

```
inherit core-image

# Force root autologin
IMAGE_AUTOLOGIN = "1"

AUTOLOGIN = "${@ 'autologin' if d.getVar('IMAGE_AUTOLOGIN') == '1' else '' }"

inherit ${AUTOLOGIN}

COMMON_FEATURES = " \
    ssh-server-dropbear \
    hwcodecs \
    debug-tweaks \
    dev-pkgs \
    tools-sdk \
    package-management \
    "
IMAGE_FEATURES += "${COMMON_FEATURES}"

SYSTEMD_DEFAULT_TARGET = "${@bb.utils.contains_any('IMAGE_INSTALL', \
    [ 'packagegroup-petalinux-x11', 'packagegroup-petalinux-weston' ], \
    'graphical.target', 'multi-user.target', d)}"

COMMON_INSTALL = " \
    tcf-agent \
    mtd-utils \
    bridge-utils \
    can-utils \
    pciutils \
    kernel-modules \
    nfs-utils \
    nfs-utils-client \
    linux-xlnx-udev-rules \
    "

IMAGE_INSTALL = " \
    packagegroup-core-boot \
    ${COMMON_INSTALL} \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    "

#Create devfs entries for initramfs(bundle) image
USE_DEVFS = "${@'0' if d.getVar('INITRAMFS_IMAGE_BUNDLE') == '1' else '1'}"
IMAGE_DEVICE_TABLES:append = " files/plnx_dev_table.txt"

# If meta-xilinx-tools is available, require libdfx
LIBDFX_RECIPE = "${@'libdfx' if 'xilinx-tools' in d.getVar('BBFILE_COLLECTIONS').split() else ''}"

COMMON_INSTALL:append:zynqmp = " ${INSTALL_ZYNQ_ZYNQMP_VERSAL} hellopm ${LIBDFX_RECIPE}"
COMMON_INSTALL:append:zynq = " ${INSTALL_ZYNQ_ZYNQMP_VERSAL}"
COMMON_INSTALL:append:versal = " ${INSTALL_ZYNQ_ZYNQMP_VERSAL} ${LIBDFX_RECIPE}"

INSTALL_ZYNQ_ZYNQMP_VERSAL = " haveged htop iperf3 meson u-boot-tools"

CORE_IMAGE_EXTRA_INSTALL:append:ultra96 = " packagegroup-base-extended"

IMAGE_LINGUAS = " "

FEATURE_PACKAGES_petalinux-base ?= "packagegroup-petalinux"
FEATURE_PACKAGES_petalinux-x11 ?= "packagegroup-petalinux-x11"
FEATURE_PACKAGES_petalinux-x11[optional] ?= "1"
FEATURE_PACKAGES_petalinux-matchbox ?= "packagegroup-petalinux-matchbox"
FEATURE_PACKAGES_petalinux-matchbox[optional] ?= "1"
FEATURE_PACKAGES_petalinux-weston ?= "packagegroup-petalinux-weston"
FEATURE_PACKAGES_petalinux-weston[optional] ?= "1"
FEATURE_PACKAGES_petalinux-self-hosted ?= "packagegroup-petalinux-self-hosted"
FEATURE_PACKAGES_petalinux-self-hosted[optional] ?= "1"
FEATURE_PACKAGES_petalinux-qt ?= "packagegroup-petalinux-qt"
FEATURE_PACKAGES_petalinux-qt[optional] ?= "1"
FEATURE_PACKAGES_petalinux-opencv ?= "packagegroup-petalinux-opencv"
FEATURE_PACKAGES_petalinux-opencv[optional] ?= "1"
FEATURE_PACKAGES_petalinux-benchmarks ?= "packagegroup-petalinux-benchmarks"
FEATURE_PACKAGES_petalinux-benchmarks[optional] ?= "1"
FEATURE_PACKAGES_petalinux-gstreamer ?= "packagegroup-petalinux-gstreamer"
FEATURE_PACKAGES_petalinux-gstreamer[optional] ?= "1"
FEATURE_PACKAGES_petalinux-audio ?= "packagegroup-petalinux-audio"
FEATURE_PACKAGES_petalinux-audio[optional] ?= "1"
FEATURE_PACKAGES_petalinux-xen ?= "packagegroup-petalinux-xen"
FEATURE_PACKAGES_petalinux-xen[optional] ?= "1"
FEATURE_PACKAGES_petalinux-openamp ?= "packagegroup-petalinux-openamp"
FEATURE_PACKAGES_petalinux-openamp[optional] ?= "1"
FEATURE_PACKAGES_petalinux-mraa ?= "packagegroup-petalinux-mraa"
FEATURE_PACKAGES_petalinux-mraa[optional] ?= "1"
FEATURE_PACKAGES_petalinux-display-debug ?= "packagegroup-petalinux-display-debug"
FEATURE_PACKAGES_petalinux-display-debug[optional] ?= "1"
FEATURE_PACKAGES_petalinux-multimedia ?= "packagegroup-petalinux-multimedia"
FEATURE_PACKAGES_petalinux-multimedia[optional] ?= "1"
FEATURE_PACKAGES_petalinux-networking-debug ?= "packagegroup-petalinux-networking-debug"
FEATURE_PACKAGES_petalinux-networking-debug[optional] ?= "1"
FEATURE_PACKAGES_petalinux-networking-stack ?= "packagegroup-petalinux-networking-stack"
FEATURE_PACKAGES_petalinux-networking-stack[optional] ?= "1"
FEATURE_PACKAGES_petalinux-python-modules ?= "packagegroup-petalinux-python-modules"
FEATURE_PACKAGES_petalinux-python-modules[optional] ?= "1"
FEATURE_PACKAGES_petalinux-qt-extended ?= "packagegroup-petalinux-qt-extended"
FEATURE_PACKAGES_petalinux-qt-extended[optional] ?= "1"
FEATURE_PACKAGES_petalinux-utils ?= "packagegroup-petalinux-utils"
FEATURE_PACKAGES_petalinux-utils[optional] ?= "1"
FEATURE_PACKAGES_petalinux-v4lutils ?= "packagegroup-petalinux-v4lutils"
FEATURE_PACKAGES_petalinux-v4lutils[optional] ?= "1"
FEATURE_PACKAGES_petalinux-lmsensors ?= "packagegroup-petalinux-lmsensors"
FEATURE_PACKAGES_petalinux-lmsensors[optional] ?= "1"
FEATURE_PACKAGES_petalinux-ultra96-webapp ?= "packagegroup-petalinux-ultra96-webapp"
FEATURE_PACKAGES_petalinux-ultra96-webapp[optional] ?= "1"
FEATURE_PACKAGES_petalinux-96boards-sensors ?= "packagegroup-petalinux-96boards-sensors"
FEATURE_PACKAGES_petalinux-96boards-sensors[optional] ?= "1"
FEATURE_PACKAGES_fpga-manager ?= "fpga-manager-script fpga-manager-util"
FEATURE_PACKAGES_fpga-manager[optional] ?= "1"
FEATURE_PACKAGES_petalinux-jupyter ?= "packagegroup-petalinux-jupyter"
FEATURE_PACKAGES_petalinux-jupyter[optional] ?= "1"
FEATURE_PACKAGES_petalinux-pynq-bnn ?= "packagegroup-petalinux-pynq-bnn"
FEATURE_PACKAGES_petalinux-pynq-bnn[optional] ?= "1"
FEATURE_PACKAGES_petalinux-pynq-96boardsoverlay ?= "packagegroup-petalinux-pynq-96boardsoverlay"
FEATURE_PACKAGES_petalinux-pynq-96boardsoverlay[optional] ?= "1"
FEATURE_PACKAGES_petalinux-pynq ?= "packagegroup-petalinux-pynq"
FEATURE_PACKAGES_petalinux-pynq[optional] ?= "1"
FEATURE_PACKAGES_petalinux-pynq-helloworld ?= "packagegroup-petalinux-pynq-helloworld"
FEATURE_PACKAGES_petalinux-pynq-helloworld[optional] ?= "1"

FEATURE_PACKAGES_petalinux-ocicontainers ?= "packagegroup-petalinux-ocicontainers"
FEATURE_PACKAGES_petalinux-ocicontainers[optional] ?= "1"
FEATURE_PACKAGES_petalinux-tpm ?= "packagegroup-petalinux-tpm"
FEATURE_PACKAGES_petalinux-tpm[optional] ?= "1"
FEATURE_PACKAGES_petalinux-ros ?= "packagegroup-petalinux-ros"
FEATURE_PACKAGES_petalinux-ros[optional] ?= "1"
```

Fill the `device-tree.bbappend` with the following content:
```
FILESEXTRAPATHS:prepend := "${THISDIR}/files:${SYSCONFIG_PATH}:"

SRC_URI:append = " file://config file://system-caribou.dtsi"
DEPENDS:append = "${@' lopper-native' if d.getVar('SYSTEM_DTFILE') != '' else ''}"

# We need the deployed output
PROC_TUNE:versal = "${@'cortexa72' if d.getVar('SYSTEM_DTFILE') != '' else ''}"
PROC_TUNE:zynqmp = "${@'cortexa53' if d.getVar('SYSTEM_DTFILE') != '' else ''}"

python () {
    if d.getVar("CONFIG_DISABLE"):
        d.setVarFlag("do_configure", "noexec", "1")
}

export PETALINUX
do_configure:append () {
    if [ ! -z "${SYSTEM_DTFILE}" ]; then
        user_dtsi="${TOPDIR}/../project-spec/decoupling-dtsi/system-caribou.dtsi"
        apu_dts="${TOPDIR}/../project-spec/decoupling-dtsi/${PROC_TUNE}-${SOC_FAMILY}-linux.dts"
	lopper -f -v --enhanced --permissive -i ${user_dtsi} ${apu_dts} system-default.dtb
    else
	script="${PETALINUX}/etc/hsm/scripts/petalinux_hsm_bridge.tcl"
	data=${PETALINUX}/etc/hsm/data/
	eval xsct -sdx -nodisp ${script} -c ${WORKDIR}/config \
	-hdf ${DT_FILES_PATH}/hardware_description.${HDF_EXT} -repo ${S} \
	-data ${data} -sw ${DT_FILES_PATH} -o ${DT_FILES_PATH} -a "soc_mapping"
    fi
}
```

Fill the `system-user.dtsi` file with the following content:
#### For ZC706

```
//include/ "system-conf.dtsi"
/ {
};

&i2c0 {
  pinctrl-names = "default";
  pinctrl-0 = <&pinctrl_i2c0_default>;

  i2c-mux@74 {
    compatible = "nxp,pca9548";
    #address-cells = <1>;
    #size-cells = <0>;
    reg = <0x74>;

    i2c@0 {
      #address-cells = <1>;
      #size-cells = <0>;
      reg = <0>;
      si570: clock-generator@5d {
        #clock-cells = <0>;
        compatible = "silabs,si570";
        temperature-stability = <50>;
        reg = <0x5d>;
        factory-fout = <156250000>;
        clock-frequency = <148500000>;
      };
    };

    i2c@1 {
      #address-cells = <1>;
      #size-cells = <0>;
      reg = <1>;
      adv7511: hdmi-tx@39 {
        compatible = "adi,adv7511";
        reg = <0x39>;
        adi,input-depth = <8>;
        adi,input-colorspace = "yuv422";
        adi,input-clock = "1x";
        adi,input-style = <3>;
        adi,input-justification = "evenly";
      };
    };

    i2c@2 {
      #address-cells = <1>;
      #size-cells = <0>;
      reg = <2>;
      eeprom@54 {
        compatible = "atmel,24c08";
        reg = <0x54>;
      };
    };

    i2c@3 {
      #address-cells = <1>;
      #size-cells = <0>;
      reg = <3>;
      gpio@21 {
        compatible = "ti,tca6416";
        reg = <0x21>;
        gpio-controller;
        #gpio-cells = <2>;
      };
    };

    i2c@4 {
      #address-cells = <1>;
      #size-cells = <0>;
      reg = <4>;
      rtc@51 {
        compatible = "nxp,pcf8563";
        reg = <0x51>;
      };
    };

    //HPC
    i2c@5 {
      #address-cells = <1>;
      #size-cells = <0>;
      reg = <5>;
      i2cswitch_hpc@71 {
        compatible = "nxp,pca9846";
        #address-cells = <1>;
        #size-cells = <0>;
        reg = <0x71>;

        i2c_hpc@0 {
          compatible = "nxp,pca9846";
          #address-cells = <1>;
          #size-cells = <0>;
          reg = <0>;
        };
      };
    };

    //LPC
    i2c@6 {
      #address-cells = <1>;
      #size-cells = <0>;
      reg = <6>;

      i2cswitch_lpc@71 {

        compatible = "nxp,pca9846";
        #address-cells = <1>;
        #size-cells = <0>;
        reg = <0x71>;

        i2c_lpc@0 {
          compatible = "nxp,pca9846";
          #address-cells = <1>;
          #size-cells = <0>;
          reg = <0>;
        };
      };
    };


    i2c@7 {
      #address-cells = <1>;
      #size-cells = <0>;
      reg = <7>;
      ucd90120@65 {
        compatible = "ti,ucd90120";
        reg = <0x65>;
      };
    };
  };
};

```

#### For ZCU102 
Coming soon
<!-- ```
/include/ "system-conf.dtsi"
/ {
      i2c@ff030000 { /* I2C1 node */
            i2c-mux@75 { /* mux at 0x75 */
                  i2c@0 { /* i2c channel 0 under this mux */
           
                        i2c-mux@71 { /* There’s a new I2C mux at 0x71 */
                              compatible = "nxp,pca9846";
                              #address-cells = <0x01>;
                              #size-cells = <0x00>;
                              reg = <0x71>;
 
 
                              i2c@0 {
                                    #address-cells = <0x01>;
                                    #size-cells = <0x00>;
                                    reg = <0x00>;
                              };
     
                              i2c@1 {
                                    #address-cells = <0x01>;
                                    #size-cells = <0x00>;
                                    reg = <0x01>;
                              };
     
                              i2c@2 {
                                    #address-cells = <0x01>;
                                    #size-cells = <0x00>;
                                    reg = <0x02>;
                              };
     
                              i2c@3 {
                                    #address-cells = <0x01>;
                                    #size-cells = <0x00>;
                                    reg = <0x03>;
                              };
                        };
                  };
            };
      };
}; -->
```

