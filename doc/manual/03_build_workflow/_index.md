---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Build Workflow"
weight: 3
---

The PetaLinux image build process is fully supported and consists of six main steps, with two additional optional steps:

1. Project Creation
2. Image Configuration
3. Image Building
4. Image Packaging
5. Image Loading
6. Board Booting
7. Serial Probing (optional)
8. Recipe and FPGA Bitfile Updates (optional)

This section provides a step-by-step guide for executing this workflow using the [Peta-Manager](https://caribou-project.docs.cern.ch/docs/03_peta-caribou/02_peta_manager/). Following these steps will allow you to build a PetaLinux image for one of the supported target boards (ZC706 or ZCU102).

### **1. Clone the Peta-Caribou project**

```shell
$ git clone https://gitlab.cern.ch/Caribou/peta-caribou.git
```

### **2. Create a Project for the Target Board**

```shell
$ ./peta-manager.py create --prj <project-name> --bsp <path-to-bsp-file>
```

This command initializes a PetaLinux project named `<project-name>` for the target board, using the specified BSP file. It also sets up the project workspace by copying all necessary recipe definitions, device tree files, and image configuration files, preparing them for further configuration.

BSP files can be downloaded from the AMD-Xilinx [downloads](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools/2023-2.html).

### **3. Configure the Image**

```shell
$ ./peta-manager.py configure --prj <project-name> 
```

This step configures the PetaLinux image based on predefined settings in the corresponding configuration file. Some parameters can be modified as needed.

#### **3.1. Update MAC address**

```shell
$ ./peta-manager.py configure --prj <project-name> --mac <mac-address>
```

Use the `--mac` argument to specify a custom MAC address (e.g., `--mac 02:0a:35:00:03:00`).

#### **3.2. Use an external hardware configuration (XSA) file**

```shell
$ ./peta-manager.py configure --prj <project-name> --xsa <xsa-file>
```

The `--xsa` argument updates the hardware configuration using an external XSA file.

### **4. Build the Image**

```shell
$ ./peta-manager.py build --prj <project-name> 
```

This step compiles the PetaLinux image using the `bitbake` utility and the Caribou-specific configuration file. The process generates the BOOT files, which will be packaged for use during the board's boot sequence.

### **5. Package the Image**

```shell
$ ./peta-manager.py package --prj <project-name> 
```

This command prepares the compiled PetaLinux image for deployment.


### **6. Load the Image onto an SD Card**

```shell
$ ./peta-manager.py load --prj <project-name> --sd <sd-device> 
```
This step partitions the SD card, creates the necessary boot folder, and copies all required files.
The `--sd` argument specifies the SD card device path (e.g., `/dev/sda`). If not provided, it defaults to `/dev/sdc`.

**Note 1 (Important)**: In the case where the Petalinux docker container is used, this command must bu ran outside of the container.\
**Note 2**: Use dmesg to identify the correct SD card device path.\
**Note 3**: Root privileges may be required (sudo).

#### **6.1. Include an FPGA Bitfile**

```shell
$ ./peta-manager.py load --prj <project-name> --sd <sd-device> --fpga <fpga-bitfile>
```

The `--fpga` argument specifies the path to the FPGA bitfile, which will be included in the boot partition.

### **7. Boot the Board**

After loading the image, insert the SD card into the target board and ensure that the board's switches are correctly set to boot from the SD card.

If the MAC address is registered on the local network for DHCP, the board should be accessible via SSH.

Once the system boots successfully—congratulations, you have a running Peta-Caribou setup!

### **8. Probe the Board's Serial Interface (Optional)**

For debugging, especially during the boot sequence, the `probe` function of Peta-Manager can be used to monitor the board’s serial output.

```shell
$ ./peta-manager.py probe --port <usb-device> 115200
```

**Note 1 (Important)**: In the case where the Petalinux docker container is used, this command must bu ran outside of the container.\
**Note 2**: Use `dmesg` to determine which serial interface device is connected (e.g., `/dev/ttyUSB0`).

### **9. Update the FPGA bitfile**

To reprogram the FPGA, copy the new bitfile to the `/boot` directory on the target board, ensuring it is named `download.bit`:

```shell
$ scp <fpga-bitfile> <target-board>:/boot/download.bit
```

Reboot the board to apply the updated bitfile.

### **10. Update recipes**

For modifying existing recipes, the Yocto `devtool` utility is used. While expert users can manually navigate to the PetaLinux project directory and use `petalinux-devtool`, the Peta-Manager provides a higher-level interface for convenience.


More details about the devtool utility can be found in the [Peta-Manager](https://caribou-project.docs.cern.ch/docs/03_peta-caribou/02_peta_manager/) documentation. 

Currently, the main supported recipes include `peary` and `eudaq`, but the tool is flexible enough to manage any manually included recipe.

#### 10.1. Modify a recipe

```shell
$ ./peta-manager.py devtool --prj <project-name> --cmd modify -n <recipe-name> <src-path>
```

This command modifies the source of the specified `<recipe-name>` to point to the provided `<src-path>`.

#### 10.2. Build a recipe

```shell
$ ./peta-manager.py devtool --prj <project-name> --cmd build <recipe-name>
```

This command initiates the build of the specified `<recipe-name>`.

**Note (Important)**: This command should only be executed if the entire PetaLinux image has already been built. Running it prematurely may cause errors during the recipe build process.

#### 10.3. Deploy recipe to target

```shell
$ ./peta-manager.py devtool --prj <project-name> --cmd deploy-target <recipe-name> <target>
```

This command transfers the updated recipe output files to the running target board.

**Note**: `<target>` is the address of the board, which must be online and accessible via SSH (e.g., `user@hostname`).

#### 10.4. Reset a recipe

```shell
$ ./peta-manager.py devtool --prj <project-name> --cmd reset <recipe-name>
```

This command removes the specified recipe and its configuration from the workspace layer. 

**Note (Important)**: This command must only be run if the recipe was already modified using the command descibed in the *Modify a recipe* step.