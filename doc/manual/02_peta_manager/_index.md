---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Peta-Manager"
weight: 2
---

## Introduction

Peta-Caribou fully supports the PetaLinux image build workflow, which consists of five main steps:

1. Project Creation
2. Image Configuration
3. Image Building
4. Image Packaging
5. Image Loading

The entire process is centrally managed by the `Peta-Caribou Manager`, an automation script designed to assist Caribou users who may not be familiar with the PetaLinux workflow. It provides a command-line interface that simplifies navigation through each step, ensuring seamless configuration and deployment of PetaLinux images for supported AMD Xilinx ZYNQ boards. 

```shell
usage: peta-manager.py [-h] {create,configure,devtool,build,package,load,probe,info} ...

Petalinux Project Automation Script

positional arguments:

    create              Create petalinux project
    configure           Configure petalinux project
    devtool             Petalinux devtool command parser
    build               Build petalinux project
    package             Package petalinux project
    load                Load petalinux image to SD card
    probe               Probe target board serial interface
    info                Get project information
```

**Note**: Currently, Peta-Manager supports only the AMD Xilinx ZC706 and ZCU102 boards. 

### Create

The `create` command initializes a PetaLinux project using the specified `<project-name>` and the Board Support Package (BSP) file for the target board. BSP files can be downloaded from the AMD-Xilinx [downloads](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools/2023-2.html). 

```shell
usage: peta-manager.py create [-h] [--prj project_name] [--dry-run] [--verbose] [--bsp bsp_file]

options:
  -h, --help          show this help message and exit
  --prj project_name  Name of the petalinux project
  --dry-run           Dry run
  --verbose           Dry run
  --bsp bsp_file      Path to petalinux BSP
```

- `--prj`: Name of the project
- `--bsp`: Path to the BSP file
- `--dry-run`: Simulate execution without running commands
- `--verbose`: Enable detailed output

### Configure 

This command configures the PetaLinux project by copying recipe definitions and configuration files into the project workspace. Additionally, it allows setting a custom MAC address for the target device and integrating a hardware description file (`*.xsa`) exported from the AMD Vivado Design Suite.

```shell
usage: peta-manager.py configure [-h] [--prj project_name] [--dry-run] [--verbose] [--xsa xsa_file] [--mac mac_address]

options:
  -h, --help          show this help message and exit
  --prj project_name  Name of the petalinux project
  --dry-run           Dry run
  --verbose           Dry run
  --xsa xsa_file      XSA hardware description file
  --mac mac_address   MAC address for ethernet interface
```

- `--prj`: Name of the project
- `--xsa`: Path to the XSA hardware description file
- `--mac`: MAC address for the Ethernet interface (e.g., 02:0a:35:00:03:00)
- `--dry-run`: Simulate execution without running commands
- `--verbose`: Enable detailed output

### Build
The `build` command initiates the PetaLinux image build process.

```shell
usage: peta-manager.py build [-h] [--prj project_name] [--dry-run] [--verbose]

options:
  -h, --help          show this help message and exit
  --prj project_name  Name of the petalinux project
  --dry-run           Dry run
  --verbose           Dry run
```

- `--prj`: Name of the project
- `--dry-run`: Simulate execution without running commands
- `--verbose`: Enable detailed output

### Devtool

The `devtool` command provides access to the Yocto devtool utility, enabling modifications and builds of specific components within the Yocto stack.

```shell
usage: peta-manager.py devtool [-h] [--prj project_name] [--dry-run] [--verbose] [--cmd ...]

options:
  -h, --help          show this help message and exit
  --prj project_name  Name of the petalinux project
  --dry-run           Dry run
  --verbose           Dry run
  --cmd ...           Petalinux devtool command
```

- `--prj`: Name of the project
- `--dry-run`: Simulate execution without running commands
- `--cmd`: Devtool command to be executed (no need for quotes)
- `--verbose`: Enable detailed output

### Package

The `package` command packages the built PetaLinux image for deployment.

```shell
usage: peta-manager.py package [-h] [--prj project_name] [--dry-run] [--verbose]

options:
  -h, --help          show this help message and exit
  --prj project_name  Name of the petalinux project
  --dry-run           Dry run
  --verbose           Dry run
```

- `--prj`: Name of the project
- `--dry-run`: Simulate execution without running commands
- `--verbose`: Enable detailed output

### Load 

The load command writes the packaged PetaLinux image onto an SD card. It handles partitioning, boot folder creation, and boot file placement. Additionally, an FPGA bitstream can be included during or after the image loading.

```shell
usage: peta-manager.py load [-h] [--prj project_name] [--dry-run] [--verbose] [--sd sd_device] [--fpga fpga_bitfile]

options:
  -h, --help           show this help message and exit
  --prj project_name   Name of the petalinux project
  --dry-run            Dry run
  --verbose            Dry run
  --sd sd_device       Load petalinux image to SD card
  --fpga fpga_bitfile  Load FPGA bitfile to SD card
```

- `--prj`: Name of the project
- `--sd`: Path to the SD card device (e.g., /dev/sdX)
- `--fpga`: Path to the FPGA bitfile for inclusion in the boot partition
- `--dry-run`: Simulate execution without running commands
- `--verbose`: Enable detailed output

To update the FPGA bitfile later, copy the new file to the `/boot` directory on the target board, ensuring it is named `download.bit`:

```shell
$ scp <fpga-bitfile> <target-board>:/boot/download.bit
```

Reboot the board to apply the update.

### Probe 

This command connects to the board’s serial interface via the UART USB port. Use `dmesg` to identify the correct device (e.g., `/dev/ttyUSB0`).

```shell
usage: peta-manager.py probe [-h] [--prj project_name] [--dry-run] [--verbose] [--port port speed [port speed ...]]

options:
  -h, --help            show this help message and exit
  --prj project_name    Name of the petalinux project
  --dry-run             Dry run
  --verbose             Dry run
  --port port speed [port speed ...]
                        Probe target board serial interface
```

- `--prj`: Name of the project
- `--port`: Serial port and speed settings
- `--dry-run`: Simulate execution without running commands
- `--verbose`: Enable detailed output

### Info

This command retrieves information about the board for which the project was configured.

```shell
usage: peta-manager.py info [-h] [--prj project_name] [--dry-run] [--verbose]

options:
  -h, --help          show this help message and exit
  --prj project_name  Name of the petalinux project
  --dry-run           Dry run
  --verbose           Dry run
```

- `--prj`: Name of the project
- `--dry-run`: Simulate execution without running commands
- `--verbose`: Enable detailed output
