---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Setup Environment"
weight: 1
---

### **Requirements**
- **Petalinux:** 2023.2
- **Python:** 3.11

### **Installing Petalinux**
Before getting started, PetaLinux must be installed. This tool, provided by AMD-Xilinx for their FPGA and SoC products, can be downloaded from the AMD-Xilinx [downloads portal](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html). A registered AMD-Xilinx account is required for access.

Peta-Caribou uses PetaLinux 2023.2, as it is the last version that supports the ZC706 evaluation board.

### **Using Docker image (Preferred option)**
If installing PetaLinux 2023.2 directly is not feasible, a Docker container can be used as an alternative. The [docker-petalinux2](https://github.com/carlesfernandez/docker-petalinux2) project provides a convenient solution. Setup instructions can be found on the project page, but the process can be summarized as follows:

1. Download the PetaLinux installer.
2. Place it in the `installers/` directory of the [docker-petalinux2](https://github.com/carlesfernandez/docker-petalinux2) repository. You may need to rename it to `petalinux-v<VERSION>-final-installer.run`.
3. Run the following command, replacing `<VERSION>` with the downloaded version (e.g., 2023.2):
```shell
$ ./docker_build.sh <VERSION>
```

To launch an instance of the Docker container, run:
```shell
$ /path/to/docker-petalinux2/etc/petalin2.sh
```

### **Cloning the Peta-Caribou project**

```shell
$ git clone https://gitlab.cern.ch/Caribou/peta-caribou.git
```
