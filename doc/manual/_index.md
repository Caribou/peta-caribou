---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Peta-Caribou OS"
description: "The Caribou Petalinux image building tool"
weight: 3
---

Peta-Caribou is the PetaLinux image builder for the Caribou system. Based on PetaLinux 2023.2, it provides an interface for configuring and generating a PetaLinux image for the two AMD Xilinx ZYNQ boards supported by Caribou: the [ZC706](https://www.xilinx.com/products/boards-and-kits/ek-z7-zc706-g.html) and the [ZCU102](https://www.xilinx.com/products/boards-and-kits/ek-u1-zcu102-g.html).

For details on installing PetaLinux, refer to the [Setup Environment](https://caribou-project.docs.cern.ch/docs/02_peta-caribou/01_setup_environment/) section.

At the core of Peta-Caribou is the Peta-Manager tool, an automation script that provides a command-line interface for configuring, building, loading, updating, and deploying the PetaLinux image. A detailed overview of the supported workflow and commands can be found in the [Peta-Caribou Manager](https://caribou-project.docs.cern.ch/docs/02_peta-caribou/02_peta_manager/) section.

The Peta-Caribou Git repository is available at this [link](https://gitlab.cern.ch/Caribou/peta-caribou)