## Summary

Peta-Caribou is the Caribou system Petalinux image builder. It is based on Petalinux 2023.2 and offers an interface to configure and generate a Petalinux image for the two AMD Xilinx ZYNQ boards supported by the Caribou system, namely the [ZC706](https://www.xilinx.com/products/boards-and-kits/ek-z7-zc706-g.html) and the [ZCU102](https://www.xilinx.com/products/boards-and-kits/ek-u1-zcu102-g.html). The central component of the Peta-Caribou project is the `Peta-Manager` tool. It is an automation script that provides a command interface to configure, build, load, update and deploy the Petalinux image.

More details about the workflow and the command interface can be found in the [user manual](https://caribou-project.docs.cern.ch/docs/03_peta-caribou/)

