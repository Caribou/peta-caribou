#!/usr/bin/env python3.11

import os
import argparse
import subprocess
import shutil
import platform
import time
import re

from py.Color import Color as color
import py.Utils as utils

OSTYPE = platform.system()
PC_PATH = os.getcwd()

# Default SD device
SD_DEVICE_DEFAULT = "/dev/sdc"

class PetaManager:

  _verbose = False
  _dry_run = False

  def __init__(self, verbose=False, dry_run=False):
    self._verbose = verbose
    self._dry_run = dry_run
  
  def _enter_project(self, prj_name):
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Entering project {prj_name} {color.END}")
    else:
      """Go to petalinux project directory"""
      print(f"{color.GREEN} Entering project {prj_name} {color.END}")
      # enter petalinux project directory
      os.chdir(os.path.join(PC_PATH, prj_name))
      # set board information
      self._set_project_info(prj_name)

  def _leave_project(self):
    """Go back to pera-caribou project directory"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Leaving project {color.END}")
    else:
      # leave petalinux project
      print(f"{color.GREEN} Leaving project {color.END}")
      os.chdir(PC_PATH)

  def _copy_config_files(self):
    """Copy configuration files to petalinux project"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Copying configuration files {color.END}")
    else:
      print(f"{color.GREEN} Copying configuration files: config, recipes, dtsi, uEnv {color.END}")
      # Copy petalinux configuration file
      config_src = os.path.join(PC_PATH, f"utils/config/{PC_BOARD}/config")
      config_dest = os.path.join(PC_PRJ_PATH, "project-spec/configs/config")
      shutil.copyfile(config_src, config_dest)
    
      # Copy u-boot BSP configuration
      uboot_src = os.path.join(PC_PATH, f"utils/config/{PC_BOARD}/bsp.cfg") 
      uboot_dest = os.path.join(PC_PRJ_PATH, "project-spec/meta-user/recipes-bsp/u-boot/files/bsp.cfg")
      shutil.copyfile(uboot_src, uboot_dest)

      # Copy meta-caribou layer
      layer_src = os.path.join(PC_PATH, "utils/layers/meta-caribou")
      layer_dest = os.path.join(PC_PRJ_PATH, "project-spec/meta-caribou")
      shutil.copytree(layer_src, layer_dest, ignore=shutil.ignore_patterns('recipes-bsp*'), dirs_exist_ok=True)

      # Copy device-tree i2c patches
      dt_src = os.path.join(PC_PATH, f"utils/layers/meta-caribou/recipes-bsp/device-tree/{PC_BOARD}/system-user.dtsi")
      dt_dest = os.path.join(PC_PRJ_PATH, "project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi")
      shutil.copyfile(dt_src, dt_dest)

      # Copy uEnv.txt file
      uenv_src = os.path.join(PC_PATH, "utils/scripts/uEnv.txt")
      uenv_dest = os.path.join(PC_PRJ_PATH, "uEnv.txt")
      shutil.copyfile(uenv_src, uenv_dest)
    
  def create(self, prj_name, bsp_file):
    """Create petalinux project"""
    # Check if petalinux is installed
    utils.check_petalinux(self._dry_run) 
    
    # create petalinux project 
    print(f"{color.GREEN} Creating Petalinux project: {prj_name} {color.END}")
    cmd = f"petalinux-create --type project --name {prj_name} --source {bsp_file}"
    utils.run_shell_commands([cmd], self._verbose, self._dry_run)

    # Set global board info
    self._set_project_info(prj_name) 
    # copy configuration files
    self._copy_config_files()
    print(f"{color.GREEN} Petalinux project created: {prj_name} {color.END}")


  def _set_project_info(self, prj_name):
    """Get board type from petalinux project configuration file"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Setting board information {color.END}")
    else:
      print(f"{color.GREEN} Setting board information {color.END}")
      board_mapping = {
            "xilinx-zc706": ("ZC706", "zynq"),
            "xilinx-zcu102": ("ZCU102", "zynqmp")
      }
      global PC_PRJ_PATH, PC_CONFIG_FILE, PC_BOARD, pc_fsbl_name
      PC_PRJ_PATH = os.path.join(PC_PATH, prj_name)
      PC_CONFIG_FILE = os.path.join(PC_PRJ_PATH, "project-spec/configs/config")
      with open(PC_CONFIG_FILE, 'r') as file:
        for line in file:
          if line.startswith("CONFIG_YOCTO_MACHINE_NAME"):
            value = line.split('=')[1].strip().strip('"')
            if value in board_mapping:
              PC_BOARD, pc_fsbl_name = board_mapping[value]
              return PC_BOARD, pc_fsbl_name
            else:
              raise ValueError(f"Unknown board type: {value}")

  def info(self, prj_name): 
    """Get project information"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Getting project information {color.END}")
    else:
      # enter petalinux project directory
      self._enter_project(prj_name)
      # print project information
      print(f"{color.YELLOW} Project information for {prj_name} {color.END}")
      print(f"{color.YELLOW} Board: {PC_BOARD} {color.END}")
      # go back to root directory
      self._leave_project()
  
  def _update_mac_address_setting(self, file_path, name, value):
    """Update MAC address in petalinux configuration file"""
    with open(file_path, 'r') as file:
      lines = file.readlines()
    with open(file_path, 'w') as file:
      for line in lines:
        if line.startswith(f"{name}="):
          if line.endswith('"\n'):
            line = f'{name}="{value}"\n'
          else:
            line = f'{name}={value}\n'
        file.write(line)

  def _update_mac_address_conf(self, mac_address):
    """Update MAC address in petalinux configuration file"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Updating MAC address {color.END}")
    else:
      print(f"{color.GREEN} Updating MAC address {color.END}")
      # Set correct mac address variable name
      mac_variable_name = ""
      if PC_BOARD == "ZCU102":
        mac_variable_name = "CONFIG_SUBSYSTEM_ETHERNET_PSU_ETHERNET_3_MAC"
      elif PC_BOARD == "ZC706":
        mac_variable_name = "CONFIG_SUBSYSTEM_ETHERNET_PS7_ETHERNET_0_MAC"

      self._update_mac_address_setting(PC_CONFIG_FILE, mac_variable_name, mac_address)
      # log
      print(f"{color.GREEN} Configuration updated with MAC address: {mac_address}{color.END}")
  
  def _update_mac_address_uenv(self, mac_address):
    """Update MAC address in uEnv.txt file"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Updating MAC address in uEnv.txt {color.END}")
    else:
      print(f"{color.GREEN} Updating MAC address in uEnv.txt {color.END}")
      # Open file and store lines
      uenv_file = os.path.join(PC_PRJ_PATH, "uEnv.txt")
      # Set correct mac address variable name
      mac_variable_name = "ethaddr"

      self._update_mac_address_setting(uenv_file, mac_variable_name, mac_address)
      # log
      print(f"{color.GREEN} uEnv.txt updated with MAC address: {mac_address}{color.END}")

  def _update_hw_configuration(self, xsa_file):
    """Initialize/Update hardware configuration"""
    # Initialize or update hardware configuration
    print(f"{color.GREEN} Updating hardware configuration using XSA file: {color.BLUE}{xsa_file} {color.END}")
    xsa_file_path = os.path.join(PC_PATH, xsa_file)
    cmd = f"petalinux-config --get-hw-description={xsa_file_path} --silentconfig"
    utils.run_shell_commands([cmd], self._verbose, self._dry_run)
    
  def _patch_libiio(self):
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Patching libiio recipe {color.END}")  
    else:
      file_path = "components/yocto/layers/meta-openembedded/meta-oe/recipes-support/libiio/libiio_git.bb"
      # Read file
      with open(file_path, "r") as file:
          content = file.read()
      # Replace using regex
      content = re.sub(r"(branch=)master", r"\1main", content)
      # Write back to file
      with open(file_path, "w") as file:
          file.write(content)

  def configure(self, prj_name, xsa_file, mac_address):
    """Configure petalinux project"""
    # Check if petalinux is installed
    utils.check_petalinux() 

    # enter petalinux project directory
    self._enter_project(prj_name)

    # Configure petalinux project
    print(f"{color.GREEN} Configuring petalinux project {color.END}")

    # update mac address
    if mac_address:
      self._update_mac_address_conf(mac_address)
      self._update_mac_address_uenv(mac_address)

    # updae hw configuration
    if xsa_file:
      self._update_hw_configuration(xsa_file)

    # configure project
    cmd = "petalinux-config --silentconfig"
    utils.run_shell_commands([cmd], self._verbose, self._dry_run)

    # patch libiio recipe
    self._patch_libiio()

    # go back to root directory
    self._leave_project()

  def devtool(self, prj_name, cmd_args):
    """Update recipe in petalinux project"""
    # Check if petalinux is installed
    utils.check_petalinux() 

    # enter petalinux project directory
    self._enter_project(prj_name)

    # Use relative path for modify command
    if cmd_args[0] == "modify":
      cmd_args[-1] = os.path.join(PC_PATH, cmd_args[-1])
    # Form devtool command
    cmd = "petalinux-devtool"
    for arg in cmd_args:
      cmd += f" {arg}"
    # Run devtool command
    utils.run_shell_commands([cmd], self._verbose, self._dry_run)

    # go back to root directory
    self._leave_project()

  def build(self, prj_name):
    """Build petalinux project"""
    # Check if petalinux is installed
    utils.check_petalinux() 

    # enter petalinux project directory
    self._enter_project(prj_name)
  
    # build petalinux project 
    print(f"{color.GREEN} Building petalinux project {color.END}")
    cmd = "petalinux-build -c caribou-image"
    utils.run_shell_commands([cmd], self._verbose, self._dry_run)

    # go back to root directory
    self._leave_project()

  def package(self, prj_name):
    """Package petalinux project"""
    # Check if petalinux is installed
    utils.check_petalinux() 

    # enter petalinux project directory
    self._enter_project(prj_name)

    # package petalinux project
    print(f"{color.GREEN} Packaging petalinux project {color.END}")
    fsbl_name = "<fsbl-name>" if self._dry_run else f"{pc_fsbl_name}"
    cmd_pkg = f"petalinux-package --force --boot --u-boot ./images/linux/u-boot.elf --fsbl ./images/linux/{fsbl_name}_fsbl.elf"
    cmd_wic = "petalinux-package --wic"

    utils.run_shell_commands([cmd_pkg, cmd_wic], self._verbose, self._dry_run)

    # go back to root directory
    self._leave_project()

  def _wipe_sd_device(self, sd_device):
    # load petalinux image to SD card and wait for dd command to finish
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Wiping SD card {sd_device} {color.END}")
    else:
      cmd_wipefs = f"sudo wipefs --force -a {sd_device}"
      utils.run_shell_commands([cmd_wipefs], self._verbose, self._dry_run)
  
  def _load_wic_image(self, sd_device, image_archive):
    # load petalinux image to SD card and wait for dd command to finish
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Loading petalinux image to SD card {sd_device} {color.END}")
    else:
      # check if wic image is provided
      image_path = ""
      if image_archive:
        image_path = os.path.join(PC_PATH, image_archive, "petalinux-sdimage.wic")
      else:
        image_path = os.path.join(PC_PRJ_PATH, "images/linux/petalinux-sdimage.wic")
      cmd_dd = f"sudo dd if={image_path} of={sd_device} bs=1M"
      cmd_sync = "sudo sync"
      # Refresh device list and unmount
      cmd_pp = "sudo partprobe"
      utils.run_shell_commands([cmd_dd, cmd_sync, cmd_pp], self._verbose, self._dry_run)

  def _get_sd_boot(self, sd_device):
    """Check if SD card device exists"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Getting boot partition of SD card {color.END}")
      return "<sd-device>"
    else:
      sd_device_boot_part = f"{sd_device}1"
      # Search for boot partition
      if os.path.exists(f"{sd_device}p1"):
        sd_device_boot_part = f"{sd_device}p1"
      elif os.path.exists(f"{sd_device}s1"):
        sd_device_boot_part = f"{sd_device}s1"
      elif os.path.exists(f"{sd_device}1"):
        sd_device_boot_part = f"{sd_device}1"
      else:
        raise ValueError(f"Non-existent boot partition for device {sd_device}")
      print(f"{color.GREEN} Found boot partition {sd_device_boot_part} {color.END}")
      return sd_device_boot_part

  def _mount_sd_boot(self, sd_device):
    """Mount SD card"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Mounting boot partition to cari_boot {color.END}")
    else:
      # Get boot partition
      sd_device_boot_part = self._get_sd_boot(sd_device)
      # Create boot directory
      cmd_mkdir = "mkdir -p cari_boot"
      # Mount boot partition
      print(f"{color.GREEN} Mounting boot partition {sd_device_boot_part} to cari_boot {color.END}")
      cmd_mount = f"sudo mount -w {sd_device_boot_part} cari_boot"

      utils.run_shell_commands([cmd_mkdir, cmd_mount], self._verbose, self._dry_run)

  def _unmount_sd_boot(self, sd_device):
    """Unmount SD card"""
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Unmounting partitions of SD card {sd_device} {color.END}")
    else:
      print(f"{color.GREEN} Unmounting partitions of SD card {sd_device} {color.END}")

      # Unmount boot partition
      cmd_umount = "sudo umount cari_boot"
      utils.run_shell_commands([cmd_umount], self._verbose, self._dry_run)
      time.sleep(5)

      # Remove boot directory
      cmd_rm = "rm -rf cari_boot"
      utils.run_shell_commands([cmd_rm], self._verbose, self._dry_run)

  def _copy_uenv_file(self, image_archive):
    """Copy uEnv.txt file to boot partition"""
    # Copy uEnv.txt file 
    print(f"{color.GREEN} Copying uEnv.txt file to boot partition {color.END}")
    uenv_src = ""
    if image_archive:
      uenv_src = os.path.join(PC_PATH, image_archive, "uEnv.txt")
    else:
      uenv_src = os.path.join(PC_PRJ_PATH, "uEnv.txt")
    uenv_dest = "cari_boot/uEnv.txt"
    cmd_uenv = f"sudo cp {uenv_src} {uenv_dest}"
    utils.run_shell_commands([cmd_uenv], self._verbose, self._dry_run)

  def _copy_fpga_bitfile(self, fpga_bitfile):
    """Copy uEnv.txt file to boot partition"""
    # Copy bitfile file 
    if self._dry_run:
      print(f"{color.YELLOW} [DRY RUN] Copying {fpga_bitfile} file to boot partition {color.END}")
    elif fpga_bitfile:
      print(f"{color.GREEN} Copying {fpga_bitfile} file to boot partition {color.END}")
      fpga_src = os.path.join(PC_PATH, fpga_bitfile)
      fpga_dest = "cari_boot/download.bit"
      cmd_fpga = f"sudo cp {fpga_src} {fpga_dest}"
      utils.run_shell_commands([cmd_fpga], self._verbose, self._dry_run)
      time.sleep(5)

  def load(self, prj_name, sd_device, image_archive, fpga_bitfile):
    """Load petalinux image to SD card"""
    # enter petalinux project directory
    if not image_archive:
      self._enter_project(prj_name)
    # load petalinux image to SD card
    print(f"{color.GREEN} Loading petalinux project to SD card into device {sd_device} {color.END}")
    # Wipe SD card
    self._wipe_sd_device(sd_device)
    # Load wic image to SD card
    self._load_wic_image(sd_device, image_archive)
    # Mount boot partition
    self._mount_sd_boot(sd_device)
    # Copy uEnv.txt file to boot partition
    self._copy_uenv_file(image_archive)
    # Copy FPGA bitfile to boot partition
    self._copy_fpga_bitfile(fpga_bitfile)
    # Unmount boot partition
    self._unmount_sd_boot(sd_device)
    # go back to root directory
    if not image_archive:
      self._leave_project()

  def probe(self, port, speed=9600):
    """Probe target board serial interface"""
    print(f"{color.GREEN} Probing serial port {port} at speed {speed} {color.END}")
    cmd = f"source {PC_PATH}/utils/scripts/serial_probe.sh {port} {speed}"
    utils.run_shell_commands([cmd], self._verbose, self._dry_run)