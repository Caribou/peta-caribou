import subprocess
import shutil
from py.Color import Color as color

def run_shell_commands(cmd_l, verbose=False, dry_run=False):
  """Run petalinux command"""
  for cmd in cmd_l:
    if dry_run:
      print(f"{color.YELLOW} [DRY-RUN] {cmd} {color.END}")
    else:
      if verbose:
        print(f"{color.BLUE} [RUN] {cmd} {color.END}")
      subprocess.run(cmd, shell=True, check=True)

def check_petalinux(dry_run=False): 
  """Check if petalinux is installed"""
  if dry_run:
    print(f"{color.YELLOW} [DRY RUN] Checking petalinux installation {color.END}")
  else:
    exec = shutil.which("petalinux-create")
    if exec is None:
      print(f"{color.RED} [ERROR] Petalinux can't be found. Please set up your environment before proceeding {color.END}")
      exit(1)