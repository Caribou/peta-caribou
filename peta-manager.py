#!/usr/bin/env python3.11

import argparse
from py.Color import Color as color
import py.PetaManager as pm
from py.PetaManager import SD_DEVICE_DEFAULT

def main():
  parser = argparse.ArgumentParser(description='Petalinux Project Automation Script')
  subparsers = parser.add_subparsers(dest='command', help='Sub-command help')

  # Global
  global_parser = argparse.ArgumentParser(add_help=False)
  global_parser.add_argument('--prj', metavar='name', help='Name of the petalinux project')
  global_parser.add_argument('--dry-run', action='store_true', default=False, help='Dry run')
  global_parser.add_argument('--verbose', action='store_true', default=False, help='Verbose output')

  # Creation
  create_parser = subparsers.add_parser('create', parents=[global_parser], help='Create petalinux project')
  create_parser.add_argument('--bsp', metavar='<file>', help='Path to petalinux BSP')

  # Configuration
  config_parser = subparsers.add_parser('configure', parents=[global_parser], help='Configure petalinux project')
  config_parser.add_argument('--xsa', metavar='<file>', help='XSA hardware description file')
  config_parser.add_argument('--mac', metavar='<address>', default='02:0a:35:00:03:00', help='MAC address for ethernet interface')

  # Devtool
  devtool_parser = subparsers.add_parser('devtool', parents=[global_parser], help='Petalinux devtool command parser')
  devtool_parser.add_argument('--cmd', nargs=argparse.REMAINDER, metavar='<command>', help='Petalinux devtool command')

  # Building
  build_parser = subparsers.add_parser('build', parents=[global_parser], help='Build petalinux project') 

  # Packaging
  package_parser = subparsers.add_parser('package', parents=[global_parser], help='Package petalinux project')

  # Loading
  load_parser = subparsers.add_parser('load', parents=[global_parser], help='Load petalinux image files to SD card')
  load_parser.add_argument('--sd', metavar='<device>', default=SD_DEVICE_DEFAULT, help='Path to SD device')
  load_parser.add_argument('--image', metavar='<folder>', help='Path to peta-caribou build image archive')
  load_parser.add_argument('--fpga', metavar='<file>', help='Path to the FPGA bitfile')

  # Probing
  probe_parser = subparsers.add_parser('probe', parents=[global_parser], help='Probe target board serial interface')
  probe_parser.add_argument('--port', nargs='+', metavar='<port> <baudrate>', help='Probe target board serial interface')

  # Information
  info_parser = subparsers.add_parser('info', parents=[global_parser], help='Get project information')
  
  args = parser.parse_args()
  
  assert args.command, f"{color.RED} Command is required {color.END}"

  manager = pm.PetaManager(dry_run=args.dry_run, verbose=args.verbose)

  # Create
  if args.command == 'create':
    assert args.prj, f"{color.RED} [{args.command}] Project name is required {color.END}"
    assert args.bsp, f"{color.RED} [create] BSP file is required {color.END}"
    manager.create(args.prj, args.bsp)

  # Info
  if args.command == 'info':
    assert args.prj, f"{color.RED} [{args.command}] Project name is required {color.END}"
    manager.info(args.prj)
  
  # Configure
  if args.command == 'configure':
    assert args.prj, f"{color.RED} [{args.command}] Project name is required {color.END}"
    manager.configure(args.prj, xsa_file=args.xsa, mac_address=args.mac)
  
  # Devtool 
  if args.command == 'devtool':
    assert args.prj, f"{color.RED} [{args.command}] Project name is required {color.END}"
    manager.devtool(args.prj, cmd_args=args.cmd)

  # Build
  if args.command == 'build':
    assert args.prj, f"{color.RED} [{args.command}] Project name is required {color.END}"
    manager.build(args.prj)
  
  # Package
  if args.command == 'package':
    assert args.prj, f"{color.RED} [{args.command}] Project name is required {color.END}"
    manager.package(args.prj)

  # Load
  if args.command == 'load':
    if not args.image:
      assert args.prj, f"{color.RED} [{args.command}] Project name is required {color.END}"
    assert args.sd, f"{color.RED} [load] SD device is required {color.END}"
    manager.load(args.prj, sd_device=args.sd, image_archive=args.image, fpga_bitfile=args.fpga)

  # Probe
  if args.command == 'probe':
    assert args.port[0] and args.port[1], f"{color.RED} [probe] Serial port and speed are required {color.END}"
    manager.probe(port=args.port[0], speed=args.port[1])

# Main
if __name__ == '__main__':
  try:
    main()
  except AssertionError as e:
    print(e)
